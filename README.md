# ts-spike-serverless


## Getting started

AWS provide a bunch of tools and utilities to create typescript projects.

This repo is a simple hello world function deployed via cloudformation.

Continue reading and have a look at the code to understand how TS can be implemented in your serverless project.

It is important to notice that npm run compile will transpile your code

tsconfig.file creates a new folder (built) with the code tanspiled. Have a look at the handler in the template to understand the full integration

It is already deployed but if you like to (remember grant all permissions IAM and policies) otherwise:

From the browser (you can pass query params as you can see below whilst testing from CLI)

https://qnqu8yt2nf.execute-api.us-east-1.amazonaws.com/Prod/hello/
## Deploy
```
$ cd hello-world
$ npm run compile
$ cd ..
$ sam deploy
```

## Test 
```
$ curl https://<endpoint id>.execute-api.us-east-1.amazonaws.com/Prod/hello/?a=1
Queries: {"a":"1"}

```
## Authors and acknowledgment
Ivan Alberdi

